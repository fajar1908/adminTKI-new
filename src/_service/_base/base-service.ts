import { Http, RequestOptions, Headers } from "@angular/http";
import { IEnv } from "../../ienv/ienv";
import { Injector } from "@angular/core/src/di/injector";
import { Observable } from 'rxjs/Observable';
// import { environment } from '../../../environments/environment';
import 'rxjs/Rx';
import { Login } from "src/_model/login";
export abstract class BaseService {
    protected http: Http
    apiUrl: string
    comUrl:string
    apiTransactionUrl: string;
    environment: IEnv = new IEnv();


    constructor(injector: Injector, path?: string, apiUrl?: string) {
        this.http = injector.get(Http)
        if (apiUrl) {
            this.apiUrl = apiUrl
        } else {
            this.apiUrl = this.environment.ienv.http + this.environment.ienv.apiUrl
            this.comUrl = this.environment.ienv.http + this.environment.ienv.comUrl
        }

        // if (path) {
        //     path = path.slice(-1) == '/' ? path : path.concat('/')
        //     path = path.slice(0, 1) == '/' ? path.slice(1) : path
        //     this.apiUrl += path
        //     this.apiTransactionUrl += path
        //   }
    }

    saveHttp(object: object) {
        let headers = new Headers({ 
            'Content-Type': 'application/json'
         });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.apiUrl.concat('reg/register'), object, options).map(res => res.json())
    }

    loadList() {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.comUrl.concat('province/list/'), {"source":"WEB","sourceDesc":"bisnisq.com"}, options)
          .map(res => res.json())
      }
    loadListCity(id:Object) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.comUrl.concat('city/list/'), {"source":"WEB","sourceDesc":"bisnisq.com","provinceId":id}, options)
            .map(res => res.json())
    }

    getActiv(object: object) {
        let headers = new Headers({ 
            'Content-Type': 'application/json'
         });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.apiUrl.concat('reg/activation'), object, options).map(res => res.json())
    }

    // getOtk(object: object) {
    //     let headers = new Headers({ 
    //         'Content-Type': 'application/json'
    //         });
    //     let options = new RequestOptions({ headers: headers });
    //     return this.http.get(this.apiUrl.concat('requestOtk'), object, options)
    //     .map(res => res.json())
    // }

    getOtk(login: Login) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.apiUrl.concat('requestOtk/'), login, options).map(res => res.json())
      }

      getLogin(login: Object) {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.apiUrl.concat('login/'), login, options).map(res => res.json())
      }

    

    
}