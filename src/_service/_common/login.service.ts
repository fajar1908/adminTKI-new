import { Injectable, Injector } from '@angular/core';
import { Http } from '@angular/http';
import { BaseService } from '../_base/base-service';

let path: string = 'login'

@Injectable()
export class LoginService extends BaseService {


  constructor(
    injector: Injector
  ) {
    super(injector, path);
  }

}
