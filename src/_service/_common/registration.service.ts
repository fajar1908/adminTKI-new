import { Injectable, Injector } from '@angular/core';
import { Http } from '@angular/http';
import { Registration } from '../../_model/registration';
import { BaseService } from '../_base/base-service';

let path: string = 'register'

@Injectable()
export class RegistrationService extends BaseService {

    selectRegistrasi:Registration = new Registration()

    get getRegister(): Registration {
      return this.selectRegistrasi
    }
  
    set setRegister(registration: Registration) {
      this.selectRegistrasi = registration
    }

  constructor(
    injector: Injector
  ) {
    super(injector, path);
  }

}
