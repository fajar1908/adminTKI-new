import { Injectable, Injector } from '@angular/core';
import { Http } from '@angular/http';
import { BaseService } from '../_base/base-service';
import { LenderPersonal } from '../../_model/lender-personal';

let path: string = 'lender-personal'

@Injectable()
export class LenderPersonalService extends BaseService {

    selectLendPersonal:LenderPersonal = new LenderPersonal()

  constructor(
    injector: Injector
  ) {
    super(injector, path);
  }

}
