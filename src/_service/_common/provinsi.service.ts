import { Injectable, Injector } from '@angular/core';
import { Http } from '@angular/http';
import { BaseService } from '../_base/base-service';
import { Provinsi } from '../../_model/provinsi';

let path: string = 'register'

@Injectable()
export class ProvinsiService extends BaseService {

    selectProvinsi:Provinsi = new Provinsi()

  constructor(
    injector: Injector
  ) {
    super(injector, path);
  }

}
