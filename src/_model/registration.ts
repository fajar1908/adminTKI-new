export class Registration {
    name:string
    email:string
    password:string
    phone:string
    address:string
    cityId:string 
    postalCode:string
    source:string = "WEB"
    sourceDesc:string = "lendtech.com"
    referenceCode:string = ""
    urlActv:string = "http://localhost:4200/#/activation"
    emailAktifasi:string
}