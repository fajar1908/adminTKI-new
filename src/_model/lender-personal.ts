import { Provinsi } from "./provinsi";
import { City } from "./city";

export class LenderPersonal{
    namaLengkap:string
    jenKel:string
    tempatLahir:string
    tanggalLahir:Date
    alamat:string
    provinsi:Provinsi
    city:City
    kodePos:string
    email:string
    noHandphone:string
    pekerjaan:string
    noKtp:string
    fileKtpBase64:string
    noNpwp:string
    fileNpwpBase64:string
    noRekening:string
    namaBank:string
}