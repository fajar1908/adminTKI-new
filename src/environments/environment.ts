// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
const http = 'http://'
const servDiary = 'localhost'
var contextPath: boolean = true
// backend server using spring-boot:run app 
const springDiary = 'localhost'

const svPort = contextPath ? ':8080' : ''
const myDiary = contextPath ? servDiary : springDiary
const masterPort = contextPath ? svPort + '/lendtech-wsaccount/service/' : ':8080'
const commonPort = contextPath ? svPort + '/lendtech-wscommon/service/' : ':8080'
export const environment = {
  production: false,
  http: http,
  apiUrl: myDiary + masterPort,
  comUrl:myDiary + commonPort
};
