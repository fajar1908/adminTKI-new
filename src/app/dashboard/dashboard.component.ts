import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { Router } from '@angular/router';
import { RouterUrl } from 'src/app/app.var';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Registration } from '../../_model/registration';
import { RegistrationService } from '../../_service/_common/registration.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  @ViewChild('openModal') openModal: ElementRef;
  isLogin: any
  routerUrl = RouterUrl;
  status: string
  statusIns: string
  registrasi: Registration = new Registration()

  flagIsi: boolean = true
  constructor(
    private router: Router,
    private registrationService: RegistrationService
  ) {

  }

  ngOnInit() {
    this.isLogin = localStorage.getItem("token")
    console.log(this.isLogin)
    // this.flagIsi = true;
    if (this.flagIsi == true) {

      this.openModal.nativeElement.click();
    }
    if (this.isLogin == 'undefined' || this.isLogin == null) {
      this.router.navigate(['/' + this.routerUrl.login])
    }
  }


  onIndividu() {
    if (this.status == "INDIVIDUAL") {
      this.router.navigate(['/' + this.routerUrl.individual])
    } else {

    }

  }

  onInstitusi() {
    this.registrationService.setRegister = this.registrasi
    this.router.navigate(['/' + this.routerUrl.institusi])
  }

}
