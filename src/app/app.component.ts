import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DOCUMENT, Location } from '@angular/common';
import { LoaderService } from 'src/_service/_common/loader.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    route: string;
    showLoader: boolean
    constructor( private location: Location,
        router: Router,
        private loaderService?: LoaderService) {
            router.events.subscribe(val => {
                this.route = location.path();
              });
    }

    ngOnInit() {
        console.log(this.location.path())
        this.loaderService.status.subscribe((val: boolean) => {
          this.showLoader = val
        })
    }
}
