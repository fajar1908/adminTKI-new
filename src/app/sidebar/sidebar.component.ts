import { Component, OnInit } from '@angular/core';
import { RouterUrl } from 'src/app/app.var';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  route:string;
  
  toggleActive: false;

  routerUrl = RouterUrl;
  menu: any
  constructor(router:Router) {
    
   }

  ngOnInit() {
  }

}
