import { Component, OnInit,ViewContainerRef } from '@angular/core';
import { RouterUrl } from 'src/app/app.var';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgProgress } from 'ngx-progressbar';
import { Activation } from 'src/_model/activation';
import { ActivationService } from 'src/_service/_common/activation.service';

@Component({
  selector: 'app-activation',
  templateUrl: './activation.component.html',
  styleUrls: ['./activation.component.css']
})
export class ActivationComponent implements OnInit {
  routerUrl = RouterUrl;
  activation:Activation = new Activation()
  constructor(
    private toastr: ToastrService,
    private activationService:ActivationService,
    private vcr: ViewContainerRef,
    private router: Router,
    private progressService: NgProgress,
  ) { }

  ngOnInit() {

    this.activation.email = JSON.parse(localStorage.getItem("email"))

  }

  onActiv(){
    this.progressService.start()
    this.activationService.getActiv(this.activation).subscribe(
      output => {
          this.progressService.done()
          this.activation = new Activation()
          if(output.technicalMsg != ""){

            this.toastr.error(output.technicalMsg, 'Major Error', {
              timeOut: 3000,
            });

          }else{

            this.toastr.success('<small>Secara otomatis Anda akan kembali ke halaman Login </small>', 'Success', {
              timeOut: 3000,
            });
            this.router.navigate(['/'+this.routerUrl.login])
          }
         
          
      },
      error => {
        console.log(error)
      }
    )
 

  }

}
