import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { RouterUrl } from 'src/app/app.var';
import { ProvinsiService } from 'src/_service/_common/provinsi.service';
import { ViewContainerRef } from '@angular/core';
import { NgProgress } from 'ngx-progressbar';
import { Provinsi } from 'src/_model/provinsi';
import { Observable } from 'rxjs/Observable';
import { FormControl } from '@angular/forms';
import { City } from 'src/_model/city';
import { Registration } from 'src/_model/registration';
import * as shajs from 'sha.js';
import { ToastrService } from 'ngx-toastr';
import { RegistrationService } from 'src/_service/_common/registration.service';
import { LenderPersonal } from '../../_model/lender-personal';

@Component({
  selector: 'app-individual',
  templateUrl: './individual.component.html',
  styleUrls: ['./individual.component.css']
})
export class IndividualComponent implements OnInit {
  @ViewChild('ktp') fileInput: ElementRef;
  @ViewChild('npwp') fileInputNpwp: ElementRef;
  routerUrl = RouterUrl;
  provinsi:Provinsi = new Provinsi()
  registrasi:Registration = new Registration()
  city:City = new City()
  lendPersonal:LenderPersonal = new LenderPersonal()
  disableRadio:boolean = false;
  allProvinsi:Provinsi [] = []
  filteredProvinsi: Observable<Provinsi[]>
  myControlProvinsi: FormControl = new FormControl
  filteredProvinsiDomisili: Observable<Provinsi[]>
  myControlProvinsiDomisili: FormControl = new FormControl

  allCity:City [] = []
  filteredCity: Observable<City[]>
  myControlCity: FormControl = new FormControl
  filteredCityDomisili : Observable<City[]>
  myControlCityDomisili: FormControl = new FormControl

  constructor(
    private router: Router,
    private toastr: ToastrService,
    private registrationService:RegistrationService,
    private provinsiService:ProvinsiService,
    private vcr: ViewContainerRef,
    private progressService: NgProgress) { }

  ngOnInit() {
    
    this.getAllProvinsi();
    this.registrasi = JSON.parse(localStorage.getItem("register"))
    this.lendPersonal = new LenderPersonal()
    // localStorage.removeItem("register")
  }

  changeProv(dataProvinsi:Provinsi){
    console.log(dataProvinsi)
    this.getAllCity(dataProvinsi.id)
  }

  changeCity(dataCity:City){
    this.registrasi.cityId = dataCity.id
  }

  getAllCity(id = "") {
    this.provinsiService.loadListCity(id).subscribe(
      output => {
        this.allProvinsi = output.list;
        this.filteredCity = this.myControlCity.valueChanges
          .startWith(null)
          .map(city => city && typeof city === "object" ? city.name : city)
          .map(name => name ? this.filterProv(name) : this.allProvinsi.slice())
        this.filteredCityDomisili = this.myControlCityDomisili.valueChanges
          .startWith(null)
          .map(city => city && typeof city === "object" ? city.name : city)
          .map(name => name ? this.filterProv(name) : this.allProvinsi.slice())
      },
      error => {
        console.log(error)
      }
    )
  }

  getAllProvinsi() {
    
    this.provinsiService.loadList().subscribe(
      output => {
        this.allProvinsi = output.list;
        this.filteredProvinsi = this.myControlProvinsi.valueChanges
          .startWith(null)
          .map(provinsi => provinsi && typeof provinsi === "object" ? provinsi.name : provinsi)
          .map(name => name ? this.filterProv(name) : this.allProvinsi.slice())
        this.filteredProvinsiDomisili = this.myControlProvinsiDomisili.valueChanges
          .startWith(null)
          .map(provinsi => provinsi && typeof provinsi === "object" ? provinsi.name : provinsi)
          .map(name => name ? this.filterProv(name) : this.allProvinsi.slice())
      },
      error => {
        console.log(error)
      }
    )
  }

  filterProv(name: string): Provinsi[] {
    return this.allProvinsi.filter(
      output => output.name.toLowerCase().indexOf(name.toLowerCase()) === 0)
  }

  displayFnProv(provinsi: Provinsi) {
    return provinsi ? provinsi.name : provinsi;
  }

  filterCity(name: string): City[] {
    return this.allCity.filter(
      output => output.name.toLowerCase().indexOf(name.toLowerCase()) === 0)
  }

  displayFnCity(city: City) {
    return city ? city.name : city;
  }

  register(){
  }

  onFileChangeKtp(event) {
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        console.log(reader.result.split(',')[1])
        
      };
    }
  }

  onFileChangeNpwp(event) {
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        console.log(reader.result.split(',')[1])
      };
    }
  }

}
