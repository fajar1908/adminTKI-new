import { NativeDateAdapter } from '@angular/material';
import { Injectable } from '@angular/core';
const SUPPORTS_INTL_API = typeof Intl !== 'undefined';

@Injectable()
export class DateFormat extends NativeDateAdapter {
  useUtcForDisplay = true;
  parse(value: any): Date | null {
    if ((typeof value === 'string') && (value.indexOf('/') > -1)) {
      const str = value.split('/');
      const date = Number(str[0]);
      const month = Number(str[1])-1;
      const year = Number(str[2]);

      return new Date(year, month, date);
    }
    var timestamp = typeof value === 'number' ? value : Date.parse(value);
    return isNaN(timestamp) ? null : new Date(timestamp);
  }
}
