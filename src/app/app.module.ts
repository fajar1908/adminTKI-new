import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation'
import { MatDatepickerModule, MatNativeDateModule, MatExpansionModule, MatCardModule, MatSlideToggleModule, MatAutocompleteModule, DateAdapter } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { routes } from './app.router';
import { HttpModule, BrowserXhr } from '@angular/http';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { DatePipe, HashLocationStrategy, LocationStrategy } from '@angular/common';

import { NgProgressModule, NgProgressBrowserXhr } from 'ngx-progressbar';
import { ChartsModule } from 'ng2-charts'
import { ToastrModule } from 'ngx-toastr';
import { ModalModule } from 'ngx-bootstrap/modal';
import {DataTableModule} from "angular-6-datatable";
import { DateFormat } from './_libs/DateFormat';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SidecontrolComponent } from './sidecontrol/sidecontrol.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { LoaderService } from 'src/_service/_common/loader.service';
import { ProvinsiService } from 'src/_service/_common/provinsi.service';
import { RegistrationService } from 'src/_service/_common/registration.service';
import { LoginService } from 'src/_service/_common/login.service';
import { ActivationComponent } from './activation/activation.component';
import { ActivationService } from 'src/_service/_common/activation.service';
import { IndividualComponent } from './individual/individual.component';
import { InstitusiComponent } from './institusi/institusi.component';
import { PerusahaanComponent } from './perusahaan/perusahaan.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    DashboardComponent,
    SidecontrolComponent,
    LoginComponent,
    SignupComponent,
    ActivationComponent,
    IndividualComponent,
    InstitusiComponent,
    PerusahaanComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    CustomFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatExpansionModule,
    MatSlideToggleModule,
    MatCardModule,
    BrowserAnimationsModule,
    routes,
    HttpModule,
    DataTableModule,
    NgProgressModule,
    ScrollToModule.forRoot(),
    ChartsModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    ModalModule.forRoot(),
  ],
  providers: [
    RegistrationService,
    LoginService,
    LoaderService,
    ProvinsiService,
    ActivationService,
    { provide: DateAdapter, useClass: DateFormat },
    { provide: BrowserXhr, useClass: NgProgressBrowserXhr },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
