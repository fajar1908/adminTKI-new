import { Component, OnInit } from '@angular/core';
import { RouterUrl } from 'src/app/app.var';
import { Router } from '@angular/router';
import { ViewContainerRef } from '@angular/core';
import { NgProgress } from 'ngx-progressbar';
import { ToastrService } from 'ngx-toastr';
import { Login } from 'src/_model/login';
import { LoginService } from 'src/_service/_common/login.service';
import * as shajs from 'sha.js';
import { Registration } from '../../_model/registration';
import { RegistrationService } from '../../_service/_common/registration.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  routerUrl = RouterUrl;
  login:Login = new Login()
  register:Registration = new Registration()
  constructor(
      
      private loginService:LoginService,
      private registrationService:RegistrationService,
      private toastr: ToastrService,
      private vcr: ViewContainerRef,
      private router: Router,
      private progressService: NgProgress,

  ) {
  }

  ngOnInit() {
      localStorage.clear()
  }

  onLoggedin() {
      this.progressService.start()
      this.loginService.getOtk(this.login).subscribe(
          output => {
              if(output.errorCode != 0){
                  this.progressService.done()
                  this.login = new Login()
                  this.toastr.error(output.technicalMsg, 'Major Error', {
                    timeOut: 3000,
                  });
                  this.router.navigate(['/'+this.routerUrl.login])
              }
             
              let otk = output.otk
              let pass =  shajs('sha256').update(this.login.password).digest('hex')
              this.login.password = shajs('sha256').update(pass+otk).digest('hex')
              this.loginService.getLogin(this.login).subscribe(
                  output => {
                    this.progressService.done()
                     this.login = new Login()

                      if(output.errorCode != 0){
                          
                          // this.login = new Login()
                          this.toastr.error(output.technicalMsg, 'Major Error', {
                            timeOut: 3000,
                          });
                          this.router.navigate(['/'+this.routerUrl.login])

                      }else{

                          localStorage.setItem("token", JSON.stringify(output.token))
                          localStorage.setItem("tokenExpired", JSON.stringify(output.tokenExpire))
                          this.progressService.done()
                          this.register = output
                          localStorage.setItem("register", JSON.stringify(output))
                          console.log(this.register)
                          this.registrationService.setRegister = this.register
                          this.router.navigate(['/'+this.routerUrl.dashboard])
                      }

                     

                  },error =>{
                      console.log(error)
                  }
              )
          },
          error => {
            console.log(error)
          }
        )
     
  }
  onRegister(){
      console.log("masuk")
      this.router.navigate(['/signup'])
  }
}