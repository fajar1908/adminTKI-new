import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstitusiComponent } from './institusi.component';

describe('InstitusiComponent', () => {
  let component: InstitusiComponent;
  let fixture: ComponentFixture<InstitusiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstitusiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstitusiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
