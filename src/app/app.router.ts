import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { RouterUrl } from './app.var';
import { Http } from '@angular/http';
import { LoginComponent } from 'src/app/login/login.component';
import { SignupComponent } from 'src/app/signup/signup.component';
import { ActivationComponent } from 'src/app/activation/activation.component';
import { DashboardComponent } from 'src/app/dashboard/dashboard.component';
import { IndividualComponent } from 'src/app/individual/individual.component';
import { InstitusiComponent } from 'src/app/institusi/institusi.component';

export const router: Routes = [
    {
        path: '',
        redirectTo: '/login',
        pathMatch: 'full',
        data: {}
    },
    // {
    //     path: 'apps',
    //     canActivate: [RoleGuard],
    //     component: AppComponent,
    // },
    {
        path: RouterUrl.dashboard,
        component: DashboardComponent,
        // canActivate: [RoleGuard]
        data: {}
    },
    {
        path: RouterUrl.login,
        component: LoginComponent
    },
    {
        path: RouterUrl.signup,
        component: SignupComponent
    },
    {
        path: RouterUrl.aktivasi,
        component: ActivationComponent,
        data: {}
    },
    {
        path: RouterUrl.institusi,
        component: InstitusiComponent,
        data: {}
    },
    {
        path: RouterUrl.individual,
        component: IndividualComponent,
        data: {}
    },
    
];

export const routes: ModuleWithProviders = RouterModule.forRoot(router);
