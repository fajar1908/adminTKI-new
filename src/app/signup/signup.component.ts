import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RouterUrl } from 'src/app/app.var';
import { ProvinsiService } from 'src/_service/_common/provinsi.service';
import { ViewContainerRef } from '@angular/core';
import { NgProgress } from 'ngx-progressbar';
import { Provinsi } from 'src/_model/provinsi';
import { Observable } from 'rxjs/Observable';
import { FormControl } from '@angular/forms';
import { City } from 'src/_model/city';
import { Registration } from 'src/_model/registration';
import * as shajs from 'sha.js';
import { ToastrService } from 'ngx-toastr';
import { RegistrationService } from 'src/_service/_common/registration.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  routerUrl = RouterUrl;
  provinsi:Provinsi = new Provinsi()
  registrasi:Registration = new Registration()
  city:City = new City()
  disableRadio:boolean = false;
  allProvinsi:Provinsi [] = []
  filteredProvinsi: Observable<Provinsi[]>
  myControlProvinsi: FormControl = new FormControl
  filteredProvinsiDomisili: Observable<Provinsi[]>
  myControlProvinsiDomisili: FormControl = new FormControl

  allCity:City [] = []
  filteredCity: Observable<City[]>
  myControlCity: FormControl = new FormControl
  filteredCityDomisili : Observable<City[]>
  myControlCityDomisili: FormControl = new FormControl

  constructor(
    private router: Router,
    private toastr: ToastrService,
    private registrationService:RegistrationService,
    private provinsiService:ProvinsiService,
    private vcr: ViewContainerRef,
    private progressService: NgProgress) { }

  ngOnInit() {

    this.getAllProvinsi();
  }

  changeProv(dataProvinsi:Provinsi){
    console.log(dataProvinsi)
    this.getAllCity(dataProvinsi.id)
  }

  changeCity(dataCity:City){
    this.registrasi.cityId = dataCity.id
  }

  getAllCity(id = "") {
    this.provinsiService.loadListCity(id).subscribe(
      output => {
        this.allProvinsi = output.list;
        this.filteredCity = this.myControlCity.valueChanges
          .startWith(null)
          .map(city => city && typeof city === "object" ? city.name : city)
          .map(name => name ? this.filterProv(name) : this.allProvinsi.slice())
        this.filteredCityDomisili = this.myControlCityDomisili.valueChanges
          .startWith(null)
          .map(city => city && typeof city === "object" ? city.name : city)
          .map(name => name ? this.filterProv(name) : this.allProvinsi.slice())
      },
      error => {
        console.log(error)
      }
    )
  }

  getAllProvinsi() {
    this.provinsiService.loadList().subscribe(
      output => {
        this.allProvinsi = output.list;
        this.filteredProvinsi = this.myControlProvinsi.valueChanges
          .startWith(null)
          .map(provinsi => provinsi && typeof provinsi === "object" ? provinsi.name : provinsi)
          .map(name => name ? this.filterProv(name) : this.allProvinsi.slice())
        this.filteredProvinsiDomisili = this.myControlProvinsiDomisili.valueChanges
          .startWith(null)
          .map(provinsi => provinsi && typeof provinsi === "object" ? provinsi.name : provinsi)
          .map(name => name ? this.filterProv(name) : this.allProvinsi.slice())
      },
      error => {
        console.log(error)
      }
    )
  }

  filterProv(name: string): Provinsi[] {
    return this.allProvinsi.filter(
      output => output.name.toLowerCase().indexOf(name.toLowerCase()) === 0)
  }

  displayFnProv(provinsi: Provinsi) {
    return provinsi ? provinsi.name : provinsi;
  }

  filterCity(name: string): City[] {
    return this.allCity.filter(
      output => output.name.toLowerCase().indexOf(name.toLowerCase()) === 0)
  }

  displayFnCity(city: City) {
    return city ? city.name : city;
  }

  onSave() {
        
    //wait for fixing session
    // this.progressService.animated
    // var data = cripto.SHA256('cel')
    // console.log(data)
    let ap = shajs('sha256').update(this.registrasi.password).digest('hex')
    console.log(ap)
    this.registrasi.password = ap
    this.registrasi.phone = "+62"+this.registrasi.phone
    this.progressService.start()
    this.registrationService.saveHttp(this.registrasi)
    .subscribe(
      output => {
        this.progressService.done()
        if(output.technicalMsg != ""){                
          this.toastr.error(output.technicalMsg, 'Major Error', {
            timeOut: 3000,
          });
            this.router.navigate(['/'+this.routerUrl.signup])

        }else{

            this.toastr.clear()
            localStorage.setItem("email", JSON.stringify(this.registrasi.email))
            this.registrasi = new Registration()
            this.toastr.success(output.technicalMsg, 'Success', {
              timeOut: 3000,
            });
            // [routerLink]="['/dashboard']"
            this.router.navigate(['/'+this.routerUrl.aktivasi])
            // this.router.navigate(['/'+this.routerUrl.login])

        }
        
      }, error => {
        console.log('Something Wrong')
        console.log(error)
        this.registrasi = new Registration()
        this.toastr.clear()
       
        this.toastr.success('<small>Secara otomatis Anda akan kembali ke halaman utama </small>', 'Major Error', {
          timeOut: 3000,
        });
      }
    )
  }

  onLogin(){
    console.log("masuk")
    this.router.navigate(['/login'])
}

}
